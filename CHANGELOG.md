## [1.2.1](https://gitlab.com/to-be-continuous/gradle/compare/1.2.0...1.2.1) (2021-09-03)

### Bug Fixes

* Change boolean variable behaviour ([dfee6cb](https://gitlab.com/to-be-continuous/gradle/commit/dfee6cb401358e46316a1d7e018cfa6bb03897c7))

## [1.2.0](https://gitlab.com/to-be-continuous/gradle/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([ccab558](https://gitlab.com/to-be-continuous/gradle/commit/ccab5588dff93aaed5528a2c10c5aa9a6ddaba47))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/gradle/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([76b028c](https://gitlab.com/Orange-OpenSource/tbc/gradle/commit/76b028cd71852e2e42ef945f5eb556a02214a65d))

## 1.0.0 (2021-05-06)

### Features

* initial release ([4c573b7](https://gitlab.com/Orange-OpenSource/tbc/gradle/commit/4c573b7b8a8017c449cf6ae70e92830d989ceced))

#!/usr/bin/env bats
load "$BATS_LIBRARIES_DIR/bats-support/load.bash"
load "$BATS_LIBRARIES_DIR/bats-assert/load.bash"

function setup() {
  export TTMP=$(mktemp -d)
  sed -n '/BEGSCRIPT/,/ENDSCRIPT/p' "$CI_PROJECT_DIR/templates/gitlab-ci-gradle.yml" | sed 's/^  //' >> $TTMP/script.sh
  source $TTMP/script.sh

  export GRADLE_USER_HOME=$TTMP
}

function teardown() {
  echo -
}

@test "no provided gradle.properties ends with a fresh gradle.properties  generated from environment variables" {
    # GIVEN
    rm -f $GRADLE_USER_HOME/gradle.properties
    export http_proxy="http://the-http-proxy.com:8080"
    export https_proxy="http://the-https-proxy.com:8081"
    export no_proxy="local.site, .company.site"

    # WHEN
    handle_proxies

    # THEN
    run cat $GRADLE_USER_HOME/gradle.properties

    assert_line "systemProp.http.proxyHost=the-http-proxy.com"
    assert_line "systemProp.http.proxyPort=8080"
    assert_line "systemProp.http.nonProxyHosts=local.site|*.company.site"
    assert_line "systemProp.https.proxyHost=the-https-proxy.com"
    assert_line "systemProp.https.proxyPort=8081"
    assert_line "systemProp.https.nonProxyHosts=local.site|*.company.site"
}

@test "provided gradle.properties without http proxy ends valued without overriding existing" {
    # GIVEN
    rm -f $GRADLE_USER_HOME/gradle.properties
    echo "
systemProp.http.nonProxyHosts=do-not.overwrite
systemProp.https.proxyHost=my-own-proxy
systemProp.https.proxyPort=666
systemProp.https.nonProxyHosts=do-not.overwrite
" > $GRADLE_USER_HOME/gradle.properties
    export http_proxy="http://the-http-proxy.com:8080"
    export https_proxy="http://the-https-proxy.com:8081"
    export no_proxy="local.site, .company.site"

    # WHEN
    handle_proxies

    # THEN
    run cat $GRADLE_USER_HOME/gradle.properties

    assert_line "systemProp.http.proxyHost=the-http-proxy.com"
    assert_line "systemProp.http.proxyPort=8080"
    refute_line "systemProp.http.nonProxyHosts=local.site|*.company.site"
    refute_line "systemProp.https.proxyHost=the-https-proxy.com"
    refute_line "systemProp.https.proxyPort=8081"
    refute_line "systemProp.https.nonProxyHosts=local.site|*.company.site"
}

@test "provided gradle.properties without http no proxy ends valued without overriding existing" {
    # GIVEN
    rm -f $GRADLE_USER_HOME/gradle.properties
    echo "
systemProp.http.proxyHost=my-own-proxy
systemProp.http.proxyPort=666
systemProp.https.proxyHost=my-own-proxy
systemProp.https.proxyPort=666
systemProp.https.nonProxyHosts=do-not.overwrite
" > $GRADLE_USER_HOME/gradle.properties
    export http_proxy="http://the-http-proxy.com:8080"
    export https_proxy="http://the-https-proxy.com:8081"
    export no_proxy="local.site, .company.site"

    # WHEN
    handle_proxies

    # THEN
    run cat $GRADLE_USER_HOME/gradle.properties

    refute_line "systemProp.https.proxyHost=the-http-proxy.com"
    refute_line "systemProp.https.proxyPort=8080"
    assert_line "systemProp.http.nonProxyHosts=local.site|*.company.site"
    refute_line "systemProp.https.proxyHost=the-https-proxy.com"
    refute_line "systemProp.https.proxyPort=8081"
    refute_line "systemProp.https.nonProxyHosts=local.site|*.company.site"
}

@test "provided gradle.properties without https proxy ends valued without overriding existing" {
    # GIVEN
    rm -f $GRADLE_USER_HOME/gradle.properties
    echo "
systemProp.http.proxyHost=my-own-proxy
systemProp.http.proxyPort=666
systemProp.http.nonProxyHosts=do-not.overwrite
systemProp.https.nonProxyHosts=do-not.overwrite
" > $GRADLE_USER_HOME/gradle.properties
    export http_proxy="http://the-http-proxy.com:8080"
    export https_proxy="http://the-https-proxy.com:8081"
    export no_proxy="local.site, .company.site"

    # WHEN
    handle_proxies

    # THEN
    run cat $GRADLE_USER_HOME/gradle.properties

    refute_line "systemProp.http.proxyHost=the-http-proxy.com"
    refute_line "systemProp.http.proxyPort=8080"
    refute_line "systemProp.http.nonProxyHosts=local.site|*.company.site"
    assert_line "systemProp.https.proxyHost=the-https-proxy.com"
    assert_line "systemProp.https.proxyPort=8081"
    refute_line "systemProp.https.nonProxyHosts=local.site|*.company.site"
}

@test "provided gradle.properties without https no proxy ends valued without overriding existing" {
    # GIVEN
    rm -f $GRADLE_USER_HOME/gradle.properties
    echo "
systemProp.http.proxyHost=my-own-proxy
systemProp.http.proxyPort=666
systemProp.http.nonProxyHosts=do-not.overwrite
systemProp.https.proxyHost=my-own-proxy
systemProp.https.proxyPort=666
" > $GRADLE_USER_HOME/gradle.properties
    export http_proxy="http://the-http-proxy.com:8080"
    export https_proxy="http://the-https-proxy.com:8081"
    export no_proxy="local.site, .company.site"

    # WHEN
    handle_proxies

    # THEN
    run cat $GRADLE_USER_HOME/gradle.properties

    refute_line "systemProp.https.proxyHost=the-http-proxy.com"
    refute_line "systemProp.https.proxyPort=8080"
    refute_line "systemProp.http.nonProxyHosts=local.site|*.company.site"
    refute_line "systemProp.https.proxyHost=the-https-proxy.com"
    refute_line "systemProp.https.proxyPort=8081"
    assert_line "systemProp.https.nonProxyHosts=local.site|*.company.site"
}

